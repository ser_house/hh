<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.01.2018
 * Time: 11:44
 */

namespace App;


class Conf {
	private const FILE = ROOT_DIR . '/App/conf.ini';

	private $options = [];

	/**
	 * Conf constructor.
	 */
	public function __construct() {
		$this->options = parse_ini_file(self::FILE, true);
	}

	/**
	 * @param string $key
	 * @param null $default
	 *
	 * @return mixed|null
	 */
	public function get(string $key, $default = null) {
		if (isset($this->options[$key])) {
			return $this->options[$key];
		}

		return $this->options[$key] ?? $default;
	}

	public function getAll(): array {
		return $this->options;
	}
}


