<?php

namespace App;


use App\Storage\FileStorage;
use App\Vacancy\Specification;


/**
 * Class Controller
 */
class Controller {
	/** @var Route */
	private $route = null;


	/**
	 * Controller constructor.
	 *
	 * @param Route $route
	 */
	public function __construct(Route $route) {
		$this->route = $route;
	}

	/**
	 * Главная страница проекта и её обработка.
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function index(): string {
		if ($this->route->isAjax()) {
			$method = $_REQUEST['method'];

			switch ($method) {
				case 'change-status':
					return $this->changeStatus($_REQUEST['new_status']);

				case 'get-new':
					return $this->getNew();

				default:
					return $this->fail('Неверная операция.');
			}
		}
		else {
			return $this->main();
		}
	}

	/**
	 * Страница визуального тестирования.
	 *
	 * @return string
	 */
	public function test(): string {
		$htmls = [];

		$conf = new Conf();
		$options = $conf->getAll();

		$htmls[] = '<pre>' . print_r($options, true) . '</pre>';

		$storage = new FileStorage();
		$items = $storage->getAllStored();

		$vacancy_ini = $conf->get('vacancy');

		/** @var Specification\ISpecification[] $companySpecs */
		$companySpecs = [];
		foreach($vacancy_ini['excluded_companies'] as $company) {
			$companySpecs[] = new Specification\NotSpecification(new Specification\CompanySpecification($company));
		}

		$min_rub = (int)$vacancy_ini['min_salary_rub'];
		$min_usd = (int)$vacancy_ini['min_salary_usd'];
		$salarySpec = new Specification\SalarySpecification($min_rub, $min_usd);

		$titleSpec = new Specification\NotSpecification(new Specification\TitleSpecification($vacancy_ini['excluded_words']));

		if (!empty($items)) {

			$excluded_words_str = implode(',', $vacancy_ini['excluded_words']);

			foreach ($items as $vacancy) {

				$vacancy_card = (new \App\View\Vacancy($vacancy))->result();

				if (!$titleSpec->isSatisfiedBy($vacancy)) {
					$vacancy_card = "<div class='alert-danger' style='padding:0.3em'>Ooops! В названии вакансии нехорошее слово из <strong><em>$excluded_words_str</em></strong></div>$vacancy_card";
				}

				$currency = $vacancy->getSalary()->getCurrencyCode();
				$vacancy_card = "<div class='alert-success' style='padding:0.3em'>Currency: <strong><em>$currency</em></strong></div>$vacancy_card";

				if (!$salarySpec->isSatisfiedBy($vacancy)) {
					$amount = $vacancy->getSalary()->toString();
					$vacancy_card = "<div class='alert-danger' style='padding:0.3em'>Сумма недостаточна: <strong><em>$amount</em></strong></div>$vacancy_card";
				}

				foreach($companySpecs as $companySpec) {
					if (!$companySpec->isSatisfiedBy($vacancy)) {
						$company = $vacancy->getCompany();
						$vacancy_card = "<div class='alert-danger' style='padding:0.3em'>Ooops! Неинтересная компания <strong><em>$company</em></strong></div>$vacancy_card";
					}
				}

				$htmls[] = $vacancy_card;
			}
		}
		else {
			$htmls[] = '<div class="alert alert-danger">В хранилище ничего нет.</div>';
		}

		$menu = new Menu();
		$vars = [
			'title' => 'Тест',
			'menu' => $menu->items($this->route->getRequestUri()),
			'content' => implode('', $htmls),
		];

		return (new \App\View\Page($vars))->result();
	}

	/**
	 * Исходная главная страница.
	 *
	 * @return string
	 */
	private function main(): string {
		$menu = new Menu();
		$vars = [
			'title' => 'Вакансии',
			'menu' => $menu->items($this->route->getRequestUri()),
			'content' => (new \App\View\ManageForm())->result(),
		];

		return (new \App\View\Page($vars))->result();
	}

	/**
	 * Обработка смены состояния приложения (работать | не работать).
	 *
	 * @param string $new_status
	 *
	 * @return string
	 */
	private function changeStatus(string $new_status): string {
		$status = new Status();
		$status->setStatus($new_status);

		return json_encode([
			'status' => 'success',
			'msg' => $status->isRunning() ? 'Запущено' : 'Остановлено',
		]);
	}

	/**
	 * Обработка запроса новых вакансий.
	 *
	 * @return string
	 */
	private function getNew(): string {

		try {
			$storage = new FileStorage();
			$news = $storage->getAllNews();
			$storage->clearNews();
			$htmls = [];
			foreach ($news as $vacancy) {
				$htmls[] = (new \App\View\Vacancy($vacancy))->result();
			}

			return json_encode([
				'status' => 'success',
				'html' => $htmls,
			]);
		}
		catch (\Throwable $e) {
			return $this->fail($e->getMessage());
		}
	}

	/**
	 * Ответ о неудачном запросе.
	 *
	 * @param string $message
	 *
	 * @return string json
	 */
	private function fail(string $message): string {
		return json_encode([
			'status' => 'fail',
			'msg' => $message,
		]);
	}
}
