<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.01.2018
 * Time: 11:29
 */

namespace App;


/**
 * Class Menu.
 *
 * Простенький формирователь меню.
 *
 * @package App
 */
class Menu {
	public function items(string $current): array {
		$items = [
			'/' => [
				'title' => 'Вакансии',
				'is_active' => false,
			],
			'/test' => [
				'title' => 'Тесты',
				'is_active' => false,
			],
		];
		foreach ($items as $uri => &$item) {
			if ("/$current" == $uri) {
				$item['is_active'] = true;
				break;
			}
		}
		unset($item);

		return $items;
	}
}
