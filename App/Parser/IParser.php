<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.09.2018
 * Time: 05:31
 */

namespace App\Parser;


use App\Storage\IStorage;
use App\Vacancy\Specification\ISpecification;

/**
 * Interface IParser
 *
 * @package App\Parser
 */
interface IParser {

	/**
	 * @param IStorage $storage
	 * @param ISpecification $specification
	 *
	 * @return bool
	 */
	public function hasNew(IStorage $storage, ISpecification $specification): bool;
}
