<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.09.2018
 * Time: 05:53
 */

namespace App\Parser;


use App\Logger;

abstract class ParserBase {
	protected const CONNECT_TIMEOUT = 5;
	protected const TIMEOUT = 90;

	/** @var string $url */
	protected $url;

	/** @var Logger $logger */
	protected $logger;

	/**
	 * ParserBase constructor.
	 *
	 * @param string $url
	 */
	public function __construct(string $url) {
		$this->url = $url;

		$this->logger = new Logger('log', true);
	}


	/**
	 * Возвращает данные по конкретному адресу.
	 * Это может быть html, или, например, json.
	 *
	 * @throws \Exception
	 */
	protected function getData(): string {
		$ch = curl_init();

		$user_agent = UserAgent::getUserAgentString();

		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_NOBODY, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::CONNECT_TIMEOUT);
		curl_setopt($ch, CURLOPT_TIMEOUT, self::TIMEOUT);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		$data = curl_exec($ch);
		$errno = curl_errno($ch);

		if (empty($errno)) {
			$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if (200 != $http_code) {
				$info = curl_getinfo($ch);
				$this->logger->write($info, 'info');
			}
		}
		else {
			$error = curl_error($ch);
			$this->logger->write($error, 'error');
		}

		curl_close($ch);

		if (empty($data)) {
			throw new \Exception("Не удалось загрузить страницу $this->url");
		}

//		$logger = new Logger('data', true);
//		$debug_data = [
//			'url' => $this->url,
//			'data' => json_decode($data),
//		];
//		$logger->write($debug_data);

		return $data;
	}
}
