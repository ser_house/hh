<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.01.2018
 * Time: 12:18
 */

namespace App\Parser;

use App\Storage\IStorage;
use App\Vacancy\Salary;
use App\Vacancy\Specification\ISpecification;
use App\Vacancy\Vacancy;


class ParserHH extends ParserBase implements IParser {

	/**
	 * Получает страницу и разбирает.
	 * Если есть новые вакансии, то сохраняет их и отправляет push-уведомление.
	 * Парсер одностраничный, разбирает только первую страницу выдачи.
	 * Если потребуются остальные, то надо будет реализовать такой алгоритм:
	 *   берем первую страницу
	 *   разбираем
	 *   если встречаем вчерашнюю вакансию, то на этом все
	 *   берем следующую страницу и всё повторяем до тех пор, пока не встретится вчерашняя вакансия
	 *
	 * @param IStorage $storage
	 * @param ISpecification $specification
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function hasNew(IStorage $storage, ISpecification $specification): bool {

		$has_new = false;

		$data = $this->getData();
		$data = json_decode($data);
		foreach($data->items as $item) {

			$vacancy = $this->buildVacancy($item);

			if (!$specification->isSatisfiedBy($vacancy)) {
				continue;
			}

			if ($storage->addParsed($vacancy) && $storage->addNew($vacancy)) {
				$has_new = true;
			}
		}

		return $has_new;
	}

	/**
	 * @param $item
	 *
	 * @return Vacancy
	 */
	private function buildVacancy($item): Vacancy {
		$title = $item->name;

		$employer = $item->employer;
		$company = $employer->name;

		$city = $item->area->name;

		$itemSalary = $item->salary;
		$from = $itemSalary->from ?? 0;
		$to = $itemSalary->to ?? 0;
		$salary = new Salary($from, $to, $itemSalary->currency);

		$snippet = $item->snippet;
		$parts = [];
		if (!empty($snippet->requirement)) {
			$parts[] = $snippet->requirement;
		}

		if (!empty($snippet->responsibility)) {
			$parts[] = $snippet->responsibility;
		}
		$description = implode('<br>', $parts);

		$url = $item->alternate_url;

		return new Vacancy($title, $company, $city, $salary, $description, $url);
	}
}
