<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 28.11.2017
 * Time: 08:15
 */

namespace App\Parser;


/**
 * Class UserAgent
 *
 * @package Parser
 */
class UserAgent {
	static private $instance = null;

	private const AGENTS = [
		// 'Chrome'
		'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
		// 'Firefox'
		'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0',
		// 'IE'
		'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; MANM; .NET4.0C; .NET4.0E; MANM; rv:11.0) like Gecko',
		// 'Yandex'
		'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 YaBrowser/17.6.1.749 Yowser/2.5 Safari/537.36',
	];

	private function __construct() {
	}

	private function __clone() {
	}

	/** @noinspection PhpUnusedPrivateMethodInspection */
	private function __wakeup() {
	}

	/**
	 * @return string
	 * @throws \Exception
	 */
	public static function getUserAgentString(): string {
		if (empty(self::$instance)) {
			self::$instance = new static();
		}

		$index = random_int(0, count(self::AGENTS) - 1);

		return self::AGENTS[$index];
	}
}
