<?php


namespace App;


/**
 * Class Polling
 */
class Polling {
	private const CHANNEL = 'local_hh_main';

	/**
	 * @param $data
	 * @param string $channel
	 */
	public function send($data, string $channel = self::CHANNEL) {
		$to_send = [
			'time' => date('d.m.Y H:i:s'),
			'data' => [
				'msg' => $data,
			],
		];

		$client = new \phpcent\Client("http://localhost:8000/api");
		$client->setApiKey("Centrifugo_API_key");
		$client->publish($channel, $to_send);
	}
}
