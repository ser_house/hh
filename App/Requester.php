<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.09.2018
 * Time: 05:46
 */

namespace App;

use App\Storage\IStorage;
use App\Parser\IParser;
use App\Vacancy\Specification\ISpecification;


class Requester {

	/** @var IParser[] $parsers */
	private $parsers = [];

	/** @var Polling $poller */
	private $poller = null;

	/** @var IStorage $storage */
	private $storage = null;

	/** @var ISpecification $specification */
	private $specification;

	/**
	 * Requester constructor.
	 *
	 * @param array $parsers
	 * @param Polling $polling
	 * @param IStorage $storage
	 * @param ISpecification $specification
	 */
	public function __construct(array $parsers, Polling $polling, IStorage $storage, ISpecification $specification) {
		$this->parsers = $parsers;
		$this->poller = $polling;
		$this->storage = $storage;
		$this->specification = $specification;
	}

	public function request(): void {
		$has_new = false;
		foreach($this->parsers as $parser) {
			if ($parser->hasNew($this->storage, $this->specification)) {
				$has_new = true;
			}
		}

		if ($has_new) {
			$this->poller->send('new_vacancy');
		}
	}

	/**
	 * @return IParser[]
	 */
	public function getParsers(): array {
		return $this->parsers;
	}

	/**
	 * @return \App\Polling
	 */
	public function getPoller(): \App\Polling {
		return $this->poller;
	}
}
