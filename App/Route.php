<?php

namespace App;


/**
 * Class Route
 */
class Route {
	/** @var string */
	private $request_uri = '';
	/** @var array */
	private $args = [];
	/** @var string */
	private $root_uri = '';

	/**
	 * Route constructor.
	 */
	public function __construct() {
		$this->request_uri = ltrim($_SERVER['REQUEST_URI'], '/');

		if (empty($this->request_uri)) {
			$this->root_uri = 'index';
		}
		else {
			$this->args = explode('/', $this->request_uri);
			$this->root_uri = array_shift($this->args);
		}
	}

	public function run(): void {
		$controller = new Controller($this);
		switch ($this->root_uri) {
			case 'test':
				$result = $controller->test();
				break;

			default:
				$result = $controller->index();
				break;
		}

		print $result;
	}

	/**
	 *
	 * @return string
	 */
	public function getRequestUri(): string {
		return $this->request_uri;
	}

	/**
	 * @return bool
	 */
	public function isPost(): bool {
		return !empty($_POST);
	}

	/**
	 * @return bool
	 */
	public function isAjax(): bool {
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
	}
}
