<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.01.2018
 * Time: 14:24
 */

namespace App;


class Status {
	private $file = ROOT_DIR . '/data/status.txt';

	public const STOPPED = 'stopped';
	public const RUNNING = 'running';

	public function isRunning(): bool {
		$status = $this->getStatus();

		return self::RUNNING == $status;
	}

	public function setStatus(string $status): void {
		if (!in_array($status, [self::STOPPED, self::RUNNING])) {
			$status = self::STOPPED;
		}

		file_put_contents($this->file, $status);
	}

	private function getStatus(): string {
		if (file_exists($this->file)) {
			return file_get_contents($this->file);
		}

		file_put_contents($this->file, self::STOPPED);
		return self::STOPPED;
	}
}
