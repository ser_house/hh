<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.01.2018
 * Time: 15:05
 */

namespace App\Storage;


use App\Vacancy\Salary;
use App\Vacancy\Vacancy;

/**
 * Class FileStorage.
 *
 * Класс-репозиторий вакансий.
 *
 * @package Parser
 */
class FileStorage implements IStorage {

	/** @var string $file_storage */
	private $file_storage = '';

	/** @var string $file_storage_news */
	private $file_storage_news = '';

	/**
	 * Storage constructor.
	 */
	public function __construct() {
		$path = ROOT_DIR . '/data/storage';
		$this->file_storage = "$path/storage.txt";
		$this->file_storage_news = "$path/news.txt";
	}


	public function addParsed(Vacancy $item): bool {
		if ($this->hasParsed($item)) {
			return false;
		}

		file_put_contents($this->file_storage, json_encode($item) . PHP_EOL, FILE_APPEND);

		return true;
	}

	public function addNew(Vacancy $item): bool {
		if ($this->hasNew($item)) {
			return false;
		}

		file_put_contents($this->file_storage_news, json_encode($item) . PHP_EOL, FILE_APPEND);

		return true;
	}

	/**
	 * Возвращает все вакансии хранилища.
	 *
	 * @return Vacancy[]
	 */
	public function getAllStored(): array {
		$items = $this->getAllRawStored();

		$decoded = [];
		foreach ($items as $item) {
			$decoded_item = json_decode($item, true);
			$decoded[] = $this->buildVacancyFromRawData($decoded_item);
		}

		return $decoded;
	}

	/**
	 * Возвращает все вакансии хранилища.
	 *
	 * @return Vacancy[]
	 */
	public function getAllNews(): array {
		$items = $this->getAllRawNews();

		$decoded = [];
		foreach ($items as $item) {
			$decoded_item = json_decode($item, true);
			$decoded[] = $this->buildVacancyFromRawData($decoded_item);
		}

		return $decoded;
	}

	/**
	 * Очищает хранилище.
	 */
	public function clearStorage(): void {
		@unlink($this->file_storage);
	}

	/**
	 * Очищает хранилище.
	 */
	public function clearNews(): void {
		@unlink($this->file_storage_news);
	}

	/**
	 * Возвращает записи хранилища в том же виде, в котором они хранятся.
	 *
	 * @return array
	 */
	private function getAllRawStored(): array {
		if (!file_exists($this->file_storage)) {
			return [];
		}

		$items = file($this->file_storage);
		if (empty($items)) {
			return [];
		}

		return $items;
	}

	private function getAllRawNews(): array {
		if (!file_exists($this->file_storage_news)) {
			return [];
		}

		$items = file($this->file_storage_news);
		if (empty($items)) {
			return [];
		}

		return $items;
	}

	/**
	 * Проверяет наличие такой вакансии в хранилище.
	 *
	 * @param \App\Vacancy\Vacancy $item
	 *
	 * @return bool
	 */
	private function hasParsed(Vacancy $item): bool {
		$storage_items = $this->getAllRawStored();

		foreach ($storage_items as $storage_item) {
			$decoded_item = json_decode($storage_item, true);
			$storageVacancy = $this->buildVacancyFromRawData($decoded_item);
			if ($this->isEquals($storageVacancy, $item)) {
				return true;
			}
		}

		return false;
	}

	private function hasNew(Vacancy $item): bool {
		$storage_items = $this->getAllRawNews();

		foreach ($storage_items as $storage_item) {
			$decoded_item = json_decode($storage_item, true);
			$storageVacancy = $this->buildVacancyFromRawData($decoded_item);
			if ($this->isEquals($storageVacancy, $item)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Восстанавливает объект вакансии по данным из хранилища.
	 *
	 * @param array $raw_item
	 *
	 * @return \App\Vacancy\Vacancy
	 */
	private function buildVacancyFromRawData(array $raw_item): Vacancy {
		$salary_arr = $raw_item['salary'];

		return new Vacancy(
			$raw_item['title'],
			$raw_item['company'],
			$raw_item['city'],
			new Salary($salary_arr['min'], $salary_arr['max'], $salary_arr['currency_code']),
			$raw_item['description'],
			$raw_item['url']);
	}

	/**
	 * Сравнивает две вакансии на идентичность.
	 *
	 * @param \App\Vacancy\Vacancy $item1
	 * @param \App\Vacancy\Vacancy $item2
	 *
	 * @return bool
	 */
	private function isEquals(Vacancy $item1, Vacancy $item2): bool {
		return $item1->isEquals($item2);
	}
}
