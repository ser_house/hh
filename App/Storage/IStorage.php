<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 02.09.2018
 * Time: 05:24
 */

namespace App\Storage;


use App\Vacancy\Vacancy;

interface IStorage {
	/**
	 * Добавляет вакансию в хранилище.
	 *
	 * @param Vacancy $item
	 *
	 * @return bool
	 */
	public function addParsed(Vacancy $item): bool;

	/**
	 * Добавляет вакансию, которую надо показать как новую.
	 *
	 * @param Vacancy $item
	 *
	 * @return bool
	 */
	public function addNew(Vacancy $item): bool;

	/**
	 * Возвращает все вакансии хранилища.
	 *
	 * @return Vacancy[]
	 */
	public function getAllStored(): array;

	public function getAllNews(): array;

	/**
	 * Очищает хранилище.
	 */
	public function clearStorage(): void;

	public function clearNews(): void;
}
