<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 29.01.2018
 * Time: 15:59
 */

namespace App\Vacancy;

// @todo: вынести форматирование вон.

class Salary implements \JsonSerializable {

	/** @var int $min */
	private $min = 0;

	/** @var int $max */
	private $max = 0;

	/** @var string $currency_code */
	private $currency_code = '';


	/**
	 * Salary constructor.
	 *
	 * @param int $min
	 * @param int $max
	 * @param string $currency_code
	 */
	public function __construct(int $min = 0, int $max = 0, string $currency_code = 'RUR') {
		$this->min = $min;
		$this->max = $max;
		$this->currency_code = $currency_code;
	}

	public function toString(): string {
		$parts = [];
		if (!empty($this->min)) {
			$parts[] = 'от';
			$parts[] = $this->amountFormatted($this->min);
		}

		if (!empty($this->max)) {
			$parts[] = 'до';
			$parts[] = $this->amountFormatted($this->max);
		}

		return implode(' ', $parts);
	}

	private function amountFormatted(int $amount): string {
		switch($this->currency_code) {
			case 'USD':
				return '$' . number_format($amount);

			case 'RUR':
				return number_format($amount, 0, ',', ' ') . ' р.';

			default:
				return number_format($amount, 0, ',', ' ') . "($this->currency_code)";
		}
	}

	/**
	 * Specify data which should be serialized to JSON
	 *
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize(): array {
		return [
			'min' => $this->min,
			'max' => $this->max,
			'currency_code' => $this->currency_code,
		];
	}

	/**
	 * @return int
	 */
	public function getMin(): int {
		return $this->min;
	}

	/**
	 * @return int
	 */
	public function getMax(): int {
		return $this->max;
	}

	/**
	 * @return string
	 */
	public function getCurrencyCode(): string {
		return 'RUB' === $this->currency_code ? 'RUR' : $this->currency_code;
	}

	/**
	 * @param Salary $salary
	 *
	 * @return bool
	 */
	public function isEquals(Salary $salary): bool {
		return $this->getMin() == $salary->getMin()
			&& $this->getMax() == $salary->getMax();
	}
}
