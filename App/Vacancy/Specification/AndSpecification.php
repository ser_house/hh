<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.09.2018
 * Time: 08:00
 */

namespace App\Vacancy\Specification;


use App\Vacancy\Vacancy;

class AndSpecification implements ISpecification {

	/** @var ISpecification[] $specifications */
	private $specifications;

	/**
	 * @param array $specifications
	 */
	public function __construct(array $specifications)
	{
		$this->specifications = $specifications;
	}

	/**
	 * @inheritDoc
	 */
	public function isSatisfiedBy(Vacancy $vacancy): bool {
		$satisfied = [];

		foreach ($this->specifications as $specification) {
			$satisfied[] = $specification->isSatisfiedBy($vacancy);
		}

		return !in_array(false, $satisfied);
	}
}
