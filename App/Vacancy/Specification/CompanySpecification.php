<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.09.2018
 * Time: 08:04
 */

namespace App\Vacancy\Specification;


use App\Vacancy\Vacancy;

class CompanySpecification implements ISpecification {

	/** @var string $company */
	private $company;

	/**
	 * CompanySpecification constructor.
	 *
	 * @param string $company
	 */
	public function __construct(string $company) {
		$this->company = mb_strtolower($company);
	}


	/**
	 * @inheritDoc
	 */
	public function isSatisfiedBy(Vacancy $vacancy): bool {
		return mb_strtolower($vacancy->getCompany()) === $this->company;
	}
}
