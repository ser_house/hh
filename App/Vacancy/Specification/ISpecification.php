<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.09.2018
 * Time: 07:59
 */

namespace App\Vacancy\Specification;


use App\Vacancy\Vacancy;

interface ISpecification {

	/**
	 * @param Vacancy $vacancy
	 *
	 * @return bool
	 */
	public function isSatisfiedBy(Vacancy $vacancy): bool;
}
