<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.09.2018
 * Time: 08:04
 */

namespace App\Vacancy\Specification;


use App\Vacancy\Vacancy;

class NotSpecification implements ISpecification {

	/** @var ISpecification $specification */
	private $specification;

	/**
	 * @param ISpecification $specification
	 */
	public function __construct(ISpecification $specification) {
		$this->specification = $specification;
	}

	/**
	 * @inheritDoc
	 */
	public function isSatisfiedBy(Vacancy $vacancy): bool {
		return !$this->specification->isSatisfiedBy($vacancy);
	}
}
