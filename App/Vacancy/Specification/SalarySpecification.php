<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.09.2018
 * Time: 08:04
 */

namespace App\Vacancy\Specification;


use App\Vacancy\Vacancy;

class SalarySpecification implements ISpecification {

	/** @var int $min_rub */
	private $min_rub;

	/** @var int $min_usd */
	private $min_usd;

	/**
	 * SalarySpecification constructor.
	 *
	 * @param int $min_rub
	 * @param int $min_usd
	 */
	public function __construct(int $min_rub, int $min_usd) {
		$this->min_rub = $min_rub;
		$this->min_usd = $min_usd;
	}


	/**
	 * @inheritDoc
	 */
	public function isSatisfiedBy(Vacancy $vacancy): bool {
		$salary = $vacancy->getSalary();

		$currency = $salary->getCurrencyCode();

		switch ($currency) {
			case 'RUR':
				$min = $this->min_rub;
				break;

			case 'USD':
				$min = $this->min_usd;

				break;

			// Остальные валюты не подходят.
			default:
				return false;
		}

		// Набросок, возможно условие должно быть более сложным.
		return $salary->getMin() >= $min;
	}
}
