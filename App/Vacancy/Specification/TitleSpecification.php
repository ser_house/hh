<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.09.2018
 * Time: 08:04
 */

namespace App\Vacancy\Specification;


use App\Vacancy\Vacancy;

class TitleSpecification implements ISpecification {

	/** @var string[] $excluded */
	private $excluded;

	/**
	 * TitleSpecification constructor.
	 *
	 * @param string[] $excluded
	 */
	public function __construct(array $excluded) {
		$this->excluded = $excluded;
	}


	/**
	 * @inheritDoc
	 */
	public function isSatisfiedBy(Vacancy $vacancy): bool {
		$title = $vacancy->getTitle();

		$patterns = $replacements = [];
		foreach ($this->excluded as $word) {
			$patterns[] = "/$word/i";
			$replacements[] = '';
		}
		$count = 0;
		preg_replace($patterns, $replacements, $title, 1, $count);

		return !empty($count);
	}
}
