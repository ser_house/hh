<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.01.2018
 * Time: 15:06
 */

namespace App\Vacancy;

/**
 * Class Vacancy
 */
class Vacancy implements \JsonSerializable {

	private $title = '';
	private $url = '';
	private $company = '';
	private $city = '';
	private $salary = null;
	private $description = '';

	/**
	 * Vacancy constructor.
	 *
	 * @param string $title
	 * @param string $company
	 * @param string $city
	 * @param \App\Vacancy\Salary $salary
	 * @param string $description
	 * @param string $url
	 */
	public function __construct(string $title, string $company, string $city, Salary $salary, string $description, string $url) {
		$this->title = $title;
		$this->url = $url;
		$this->company = $company;
		$this->city = $city;
		$this->salary = $salary;
		$this->description = $description;
	}

	/**
	 * Specify data which should be serialized to JSON
	 *
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize(): array {
		return [
			'title' => $this->title,
			'url' => $this->url,
			'company' => $this->company,
			'city' => $this->city,
			'salary' => $this->salary,
			'description' => $this->description,
		];
	}

	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getUrl(): string {
		return $this->url;
	}

	/**
	 * @return string
	 */
	public function getCompany(): string {
		return $this->company;
	}

	/**
	 * @return string
	 */
	public function getCity(): string {
		return $this->city;
	}

	/**
	 *
	 * @return \App\Vacancy\Salary
	 */
	public function getSalary(): Salary {
		return $this->salary;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}

	public function isEquals(Vacancy $vacancy): bool {
		return $this->getTitle() == $vacancy->getTitle() &&
			$this->getCompany() == $vacancy->getCompany() &&
			$this->getCity() == $vacancy->getCity() &&
			$this->getSalary()->isEquals($vacancy->getSalary()) &&
			$this->getDescription() == $vacancy->getDescription();
	}
}
