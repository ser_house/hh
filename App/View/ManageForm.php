<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 25.01.2018
 * Time: 11:34
 */

namespace App\View;

use App\Status;

class ManageForm {

	/**
	 *
	 * @return string
	 */
	public function result(): string {
		$status = new Status();
		$vars = [
			'is_running' => $status->isRunning(),
		];

		ob_start();
		compact('vars');
		include ROOT_DIR . '/tpl/manage_form.tpl.php';

		return ob_get_clean();
	}
}
