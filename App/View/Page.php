<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.01.2018
 * Time: 15:51
 */

namespace App\View;


class Page {

	private $vars = [];

	/**
	 * Page constructor.
	 *
	 * @param array $vars
	 */
	public function __construct(array $vars) {
		if (!isset($vars['title'])) {
			$vars['title'] = 'Вакансии';
		}

		if (!isset($vars['content'])) {
			$vars['content'] = '';
		}

		$this->vars = $vars;
	}

	public function result() {
		ob_start();

		foreach ($this->vars as $key => $value) {
			${$key} = $value;
		}
		include ROOT_DIR . '/tpl/html.tpl.php';

		return ob_get_clean();
	}
}
