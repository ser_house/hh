<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 24.01.2018
 * Time: 15:51
 */

namespace App\View;


use App\Conf;

// @todo: презентер, модель вьюхи и сама вьюха.

class Vacancy {
	/** @var \App\Vacancy\Vacancy */
	private $vacancy = null;

	/**
	 * Vacancy constructor.
	 *
	 * @param \App\Vacancy\Vacancy $vacancy
	 */
	public function __construct(\App\Vacancy\Vacancy $vacancy) {
		$this->vacancy = $vacancy;
	}

	public function result() {
		$description = $this->vacancy->getDescription();

		$conf = new Conf();
		$vacancy_ini = $conf->get('vacancy');

		foreach($vacancy_ini['alarm_words'] as $alarm_word) {
			$description = str_replace($alarm_word, "<span class='alarm-word'>$alarm_word</span>", $description);
		}

		foreach($vacancy_ini['sweet_words'] as $sweet_word) {
			$description = str_replace($sweet_word, "<span class='sweet-word'>$sweet_word</span>", $description);
		}

		$item = [
			'title' => $this->vacancy->getTitle(),
			'url' => $this->vacancy->getUrl(),
			'company' => $this->vacancy->getCompany(),
			'city' => $this->vacancy->getCity(),
			'description' => $description,
			'salary' => $this->vacancy->getSalary()->toString(),
		];

		ob_start();
		compact('item');
		include ROOT_DIR . '/tpl/vacancy.tpl.php';

		return ob_get_clean();
	}
}
