<?php


/**
 * @param string $class_name
 *
 * @throws \Exception
 */
function autoload(string $class_name) {
	$class_name = str_replace('\\', '/', $class_name);
	$class_path = ROOT_DIR . "/$class_name.php";
	if (file_exists($class_path)) {
		require_once ROOT_DIR . '/vendor/autoload.php';
		require_once $class_path;

	}
	else {
		require_once ROOT_DIR . '/vendor/autoload.php';
	}

}

spl_autoload_register('autoload');
