<?php

error_reporting(E_ALL);

include_once 'bootstrap.php';

use App\Storage\FileStorage;
use App\Conf;
use App\Status;
use App\Polling;
use App\Logger;
use App\Vacancy\Specification;

try {
	$logger = new Logger('log');

	$hour = date('H');
	$mainStorage = new FileStorage();

	$conf = new Conf();

	$base = $conf->get('base');

	if (!empty($base['timezone'])) {
		date_default_timezone_set($base['timezone']);
	}

//	$urls = $conf->get('url');
//	if (empty($urls)) {
//		$logger->write('Не указано ни одного адреса страницы для парсинга.');
//		return;
//	}

	// В первый час новых суток очищаем главное хранилище для хранения данных новых суток.
	if ($base['clear_hour'] == $hour) {
		$mainStorage->clearStorage();
	}

	if (isset($base['worked_max_hour']) && isset($base['worked_min_hour'])) {
		if ($hour > $base['worked_max_hour'] || $hour < $base['worked_min_hour']) {
			return;
		}
	}

	$status = new Status();

	if ($status->isRunning()) {
		$polling = new Polling();

		$storage = new FileStorage();

		$parser_ini = $conf->get('parser');
		$parsers = [];
		foreach($parser_ini as $class => $urls) {
			foreach($urls as $url) {
				$parsers[] = new $class($url);
			}
		}

		$vacancy_ini = $conf->get('vacancy');

		$specifications = [];
		foreach($vacancy_ini['excluded_companies'] as $company) {
			$specifications[] = new Specification\NotSpecification(new Specification\CompanySpecification($company));
		}

		$min_rub = (int)$vacancy_ini['min_salary_rub'];
		$min_usd = (int)$vacancy_ini['min_salary_usd'];
		$specifications[] = new Specification\SalarySpecification($min_rub, $min_usd);

		$specifications[] = new Specification\NotSpecification(new Specification\TitleSpecification($vacancy_ini['excluded_words']));

		$spec = new Specification\AndSpecification($specifications);
		$requester = new \App\Requester($parsers, $polling, $storage, $spec);
		$requester->request();
	}
}
catch (Throwable $e) {
	$logger = new Logger('exception', true);
	$logger->write($e->getMessage());
}
