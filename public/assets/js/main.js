const STOPPED = 'stopped';
const RUNNING = 'running';

const EVENT_NEW_VACANCY = 'new_vacancy';

(function ($) {
	var channelId = 'local_hh_main';

	// Заголовок страницы сохраним, чтобы можно было потом восстановить.
	var originalDocTitle = document.title;

	// Таймеры для моргания количеством новых вакансий в заголовке вкладки.
	var intervalId = 0;
	var countTimerId = 0;

	// Инициализируем звук для уведомления о новых данных.
	ion.sound({
		sounds: [
			{name: "bell_ring"}
		],
		path: "assets/js/ion_sound/sounds/",
		preload: true,
		multiplay: true,
		volume: 0.6
	});

	// Реакция на новые данные.
	var updatePage = function(response) {
		$.each(response.html, function(i, item) {
			$('#result').prepend(item);
		});
		ion.sound.play("bell_ring");
	};

	// Работа с заголовком вкладки - он должен моргать, чтобы на панели задач было видно.
	var updateDocumentTitle = function() {
		var vacancyCount = $('.panel').length;
		if (vacancyCount > 0) {
			document.title = (originalDocTitle + ' (-)');
			// Через полсекунды сменим тире в скобках на число.
			// Потом оно сменится обратно на тире вызовом этой же функции в интервальном колбэке.
			// Проверка, чтобы не городить кучу таймеров для одного и того же
			// (например, когда закрываем карточку).
			if (!countTimerId) {
				countTimerId = setTimeout(function(){
					document.title = (originalDocTitle + ' (' + vacancyCount + ')');
					clearTimeout(countTimerId);
					countTimerId = 0;
				}, 500);
			}
		}
		else {
			document.title = originalDocTitle;
			if (intervalId) {
				clearInterval(intervalId);
				intervalId = 0;
			}
		}
	};

	var centrifuge = new Centrifuge('ws://172.28.128.3:8000/connection/websocket');
	// centrifuge
	centrifuge.subscribe(channelId, function(message) {
		// Уведомление о наличии новых вакансий.
		if (EVENT_NEW_VACANCY === message.data.data.msg) {
			$.post(window.location, {
				method: 'get-new'
			}, function (response) {
				if ('success' === response.status) {
					updatePage(response);
					updateDocumentTitle();
					if (!intervalId) {
						intervalId = setInterval(function() {
							updateDocumentTitle();
						}, 1000);
					}
				}
			}, 'json');
		}
	});

	centrifuge.connect();


	$(document).ready(function () {

		// Удаление карточки.
		$(this).on('click', '.close', function(event) {
			event.preventDefault();
			$(this).closest('.panel').remove();

			return false;
		});

		// Остановка-запуск с клиента.
		$('#status').change(function (event) {
			$.post(window.location, {
				method: 'change-status',
				new_status: this.checked ? RUNNING : STOPPED
			}, function (response) {
				if ('success' === response.status) {
					var $msg = $('#message');
					if (!$msg.length) {
						$('#result').before($('<div id="message"/>'));
					}
					var $item = $('<div class="text-success"/>');
					$item.html(response.msg);
					$msg.html($item);
				}
			}, 'json');
		});

	});

})(jQuery);
