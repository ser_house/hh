<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.01.2018
 * Time: 11:24
 */


require_once '../bootstrap.php';

$route = new App\Route();
$route->run();
