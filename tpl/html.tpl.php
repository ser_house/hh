<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link media='all' href="/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link media='all' href="/assets/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">
	<link media='all' href='/assets/css/style.css' rel='stylesheet'>
	<title><?= $title ?></title>
</head>
<body>
<header class="container">
	<?php if (isset($menu)): ?>
		<ul class="nav nav-pills">
			<?php foreach ($menu as $href => $item): ?>
				<li><a class="nav-link<?php if ($item['is_active']) print ' active'; ?>" href="<?= $href ?>"><?= $item['title'] ?></a></li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
</header>
<div class="container">
	<div id="content" class="row">
		<?php if (isset($content)) print $content; ?>
	</div>
	<div id="result" class="row"></div>
</div>

<script src='/assets/js/jquery-3.2.1.min.js' type='text/javascript'></script>
<script src="/assets/bootstrap/js/bootstrap.min.js"></script>
<script src='/assets/js/centrifuge.js' type='text/javascript'></script>
<script src='/assets/js/ion_sound/ion.sound.min.js' type='text/javascript'></script>
<script src='/assets/js/main.js' type='text/javascript'></script>
</body>
</html>
