<div class="panel panel-info">
	<div class="service-info">
		<span class="date-time">Время вывода: <?= date('d.m.Y H:i:s'); ?></span>
		<button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	</div>
	<div class="panel-heading">
		<h4><a href="<?= $item['url']; ?>"><?= $item['title']; ?></a></h4>
	</div>
	<div class="panel-body">
		<h4 class="text-muted"><?= $item['city']; ?> (<?= $item['company']; ?>)</h4>
		<div class="description"><?= $item['description']; ?></div>
	</div>
	<div class="panel-footer clearfix">
		<div class="money pull-right"><?= $item['salary']; ?></div>
	</div>
</div>
